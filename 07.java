public void handleIntegers(Iterable<Integer> ints) {
	int sum = StreamSupport.stream(ints.spliterator(), false).reduce(0, (a, b) -> a + b);
	if(sum < 1000) {
		((Collection)ints).add(sum);
	}
}