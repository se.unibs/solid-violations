class PersonController {
  PersonService service = PersonService.getInstance();

  void handleRequest(... ) {
      ...
      service.doSomething(...);
   }

}

class PersonService {
  PersonRepository repo = JdbcPersonRepository.getInstance()

  void doSomething(...) {
      ...
      repo.insert(new Person(...))
  }
}

class JdbcPersonRepository {
  DataSource dataSource = Environment.getDataSource();
  void insert(Person p) {
    var conn = dataSource.getConnection()
    var stat = conn.prepareStatement(...)
    ...
    stat.execute()
  }
}