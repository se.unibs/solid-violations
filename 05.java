interface FileSystem {
  void save(InputStream data, String id)
  InputStream read(String id)
  void delete(String id)
}

class FileCleaner {
    private final FileSystem fs;
    public FileCleaner(FileSystem fs) { this.fs = fs }

    public void doJob() {
       for(String fileName : getRemovableFileNames()) {
          fs.delete(fileName);
       }
    }
}

class FileUploader {
	private final FileSystem fs;
    public FileUploader(FileSystem fs) { this.fs = fs }

    public void doJob() {
       var id = buildFileIdByCurrentDateTime();
       var stream = readDataToUpload();
       fs.save(stream, id);
    }
}

class FileProvider {
	private final FileSystem fs;
    public FileProvider(FileSystem fs) { this.fs = fs }

    public FileResponse handle(FileRequest request) {
    	var stream = fs.read(request.fieldId);
    	var checksum = calculateChecksum(stream);
    	return new FileResponse(request.fileId, stream, checksum);
    }
}