int sumInts(ArrayList<Integer> ints) {
	int sum = 0;
	for(Integer i : ints) {
		sum += i;
	}
	return sum;
}