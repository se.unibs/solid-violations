Vector<String> sortStrings(Collection<String> input) {
	Vector<String> toBeSorted = new Vector<String>(input);
	Vector<String> sorted = applySomethingLikeQuickSort(toBeSorted);
	return sorted;
}