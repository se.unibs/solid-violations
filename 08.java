class Item { ... }
class Book extends Item { ... }
class Comic extends Item { ... }
class MusicAlbum extends Item { ... }



var bookshelf = new Bookshelf();
bookshelf.add(new Book("Refactoring", "Martin Fowler", ...));
bookshelf.add(new Comic("Nathan Never #50 - La biblioteca di Babele", "Antonio Serra", ...));
bookshelf.add(new Book("Mr Gwyn", "Alessandro Baricco"));
bookshelf.add(new MusicAlbum("Anime salve", "Fabrizio de André", new Song[] { ... }));


class BookShelf {
	private Collection<Item> items = new HashSet<Item>();

	void add(Item item) {
		items.add(item);
	}

	String toString() {
		var sb = new StringBuilder();
		for(Item item : items) {
			if(item instanceof Book) {
				sb.append(bookToString((Book)item));
			} else if (item instanceof Comic) {
				sb.append(comicToString((Comic)item));
			} else if (item instanceof MusicAlbum) {
				sb.append(albumcToString((MusicAlbum)item));
			} else ...

		}
		return sb.toString();
	}
}