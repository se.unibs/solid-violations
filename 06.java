interface FrontEndcontext { ... }
interface BackEndContext { ... }
class Context implements FrontEndContext, BackEndContext { ... }

public void backEndEntryPoint(FrontEndContext ctx, ... ) {
   var context = (BackEndContext)ctx
   ...
}