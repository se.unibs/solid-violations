class CachingConfigurationRepository {
	private final ItemCache cache = new InMemoryItemCache();
	
	public ConfigItem getItem(String key) {
		if(!cache.containsItem(key)) {
			var item = buildConfigurationItem(key);
			cache.addToCache(key, item);
		}
		return cache.getItem(key);
	}
}